//
// Created by SERGEY SUVOROV on 24/04/2022.
//

#include "Application.hxx"

#include <chrono>

using namespace boost::log::trivial;

Application::Application() {
    namespace attrs = boost::log::attributes;
    slg_.add_attribute("Tag", attrs::constant<std::string >("App"));
}

void Application::init() {
    Logging::init();
    requests_ = std::make_shared<server_t::requests_list>();

    server_ = std::make_shared<server_t>(requests_);
    if (server_->init() != 0) {
        BOOST_LOG_SEV(slg_, error) << "Server init fail";
        exit(1);
    }

    db_ = std::make_shared<DataBase>();
    if (!db_->init(std::getenv("DB_URL"))) {
        BOOST_LOG_SEV(slg_, error) << "Data base init fail";
        exit(1);
    }
}

ResponseHttp Application::update(const RequestHttp::dataMap& data) {
    auto nom =  std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    char mbstr[100];
    std::strftime(mbstr, sizeof(mbstr), "%Y-%m-%d %H:%M:%S", std::localtime(&nom));
    std::string collect = "";
    for (auto c : mbstr) {
        if (c == '\0') {
            break;
        }
        collect += c;
    }
    std::string sql = "INSERT INTO access(DATE) " \
                          "VALUES ('" + collect + "')";
    db_->update(sql);
    return {200, "Application status updated"};
}

void Application::addEndpoint(std::string addr,
                              methodVec vec,
                              endpoint_func_t func) {
    endpoints_.insert({std::move(addr),
                       std::make_unique<Endpoint>(std::move(vec),
                                                  std::move(func))});
}

void Application::run() {
    serverThread_ = std::thread(&server_t::run, server_);
    serverThread_.detach();
    run_ = true;
    appThread = std::thread([this](){
        while (run_) {
            process(requests_->pop());
        }
    });
}

void Application::stop() {
    run_ = false;
    BOOST_LOG_SEV(slg_, info) << "Stopping app...";
    if (appThread.joinable()) {
        appThread.join();
    }
}

void Application::process(server_t::request_type request) {
    std::stringstream ss;
    ss << request.data_;
    BOOST_LOG_SEV(slg_, info) << " Addr: " << request.uri_
                              << " Data: " << ss.str();

    // look for endpoint
    auto endpoint = endpoints_.find(request.uri_);
    // endpoint not found
    if (endpoint == endpoints_.end()) {
        server_->response(request.socket_,
                          ResponseHttp(404, "not found"));
    } else {
        // check for allowed methods
        if (std::find_if((*endpoint).second->types.cbegin(),
                         (*endpoint).second->types.cend(),
                         [&](auto& method) {
            return method == request.method_;
        }) == (*endpoint).second->types.cend()) {
            return;
        }
        server_->response(request.socket_,
                          (*endpoint).second->func(request.data_));
    }
}
