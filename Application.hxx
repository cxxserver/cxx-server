//
// Created by SERGEY SUVOROV on 24/04/2022.
//

#ifndef TESTSERVER__APPLICATION_HXX_
#define TESTSERVER__APPLICATION_HXX_

#include "ServerHttp.hxx"
#include "DataBase.hxx"

#include <unordered_map>
#include <utility>
#include <thread>

using server_t = ServerHttp;
using methodVec = std::vector<RequestHttp::Type>;
using endpoint_func_t = std::function<ResponseHttp(RequestHttp::dataMap data)>;

struct Endpoint;

class Application {
    std::shared_ptr<server_t> server_{nullptr};
    std::shared_ptr<DataBase> db_;

    server_t::requests_list_ptr requests_{nullptr};
    std::unordered_map<std::string, std::unique_ptr<Endpoint>> endpoints_;

    /// condition to run
    bool run_{true};

    boost::log::sources::severity_logger<boost::log::trivial::severity_level> slg_;

    std::thread serverThread_;
    std::thread appThread;

 public:
    Application();
    void init();
    void run();
    void stop();
    void process(server_t::request_type request);

    void addEndpoint(std::string addr,
                     methodVec vec,
                     endpoint_func_t func);

    ResponseHttp update(const RequestHttp::dataMap& data);
};

struct Endpoint {
    Endpoint(methodVec types,
             endpoint_func_t func) : types(std::move(types)),
             func(std::move(func)) {}
    methodVec types;
    endpoint_func_t func;
};


#endif //TESTSERVER__APPLICATION_HXX_
