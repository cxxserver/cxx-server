#include <iostream>

#include "Application.hxx"

ResponseHttp foo(const RequestHttp::dataMap& data = {}) {
    // process the request
    // so far, just return the current time
    auto time = std::chrono::system_clock::now();
    std::time_t end_time = std::chrono::system_clock::to_time_t(time);
    std::string s = std::ctime(&end_time);
    return {200, "Hello world<br>Now it is " + s};
}

int main(int argc, char** argv) {
    Application app;

    app.addEndpoint("/cxx/time",
                    methodVec{RequestHttp::Type::kGet},
                    foo);

    app.addEndpoint("/cxx/update",
                    methodVec{RequestHttp::Type::kGet},
                    [appPtr = &app](auto &&PH1) { return appPtr->update(std::forward<decltype(PH1)>(PH1)); });

    app.init();
    app.run();

    // for some reason `getline()` is not working if run as Linux service.
    // Use only if interactive mode is forced
    if (argc > 1 && std::strcmp(argv[1], "-i") == 0) {
        std::string command;
        std::cout << "Type `quit` to exit\n";
        std::cout << "> ";
        while (std::getline(std::cin, command)) {
            if (command == "quit") {
                app.stop();
                std::cout << "App will be terminated at next received message...\n";
                return 0;
            }
            std::cout << "> ";
        }
    } else {
        // infinite run
        while (true) {
            sleep(3600);
        }
    }
    return 0;
}
