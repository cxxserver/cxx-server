//
// Created by SERGEY SUVOROV on 02/05/2022.
//

#ifndef TESTSERVER__SERVERHTTP_HXX_
#define TESTSERVER__SERVERHTTP_HXX_

#include "ServerTcp.hxx"
#include <unordered_map>

class RequestHttp;
class ResponseHttp;

class ServerHttp : public ServerTcp<RequestHttp> {
 public:
    explicit ServerHttp(requests_list_ptr dq) : ServerTcp<RequestHttp>(dq) {}
};

struct RequestHttp {
    enum class Type {
        kGet,
        kPost,
        kDelete
    };
    using dataMap = std::unordered_map<std::string, std::string>;
    using methodMap_t = std::unordered_map<std::string, RequestHttp::Type>;
    static methodMap_t methodMap_;
    RequestHttp(int socket, const std::string& input);
    static dataMap parseData(const std::string& input);
    int socket_{0};
    Type method_;
    std::string uri_;
    dataMap data_;
};

std::ostream& operator<<(std::ostream& o, const RequestHttp::dataMap& map);

struct ResponseHttp {
    ResponseHttp( short code, std::string text) :
    code_(code),
    text_(std::move(text)) {}
    [[nodiscard]] std::string to_string() const;
    short code_;
    std::string text_;
};

#endif //TESTSERVER__SERVERHTTP_HXX_
