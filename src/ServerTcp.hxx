//
// Created by SERGEY SUVOROV on 02/05/2022.
//

#ifndef TESTSERVER__SERVERTCP_HXX_
#define TESTSERVER__SERVERTCP_HXX_


#include <utility>
#include <thread>

#include "ThreadSafeQueue.hxx"
#include "Logging.hxx"

class ResponseTcp;

/// Very basic TCP server
template <typename request_t>
class ServerTcp {
 public:
    /// alias for external access of the request type
    using request_type = request_t;
    using requests_list = ThreadSafeQueue<request_t>;
    using requests_list_ptr = std::shared_ptr<requests_list>;

 private:
    /// pointer towards the list to store the requests
    requests_list_ptr dq_;

    int sockFD_{-1};
    boost::log::sources::severity_logger<boost::log::trivial::severity_level> slg_;
 public:
    explicit ServerTcp(requests_list_ptr dq);
    int init();

    void run();
    void stop();

    template <typename response_t = ResponseTcp>
    void response(int socket, const response_t& resp);
};

struct RequestTcp {
    RequestTcp(const int socket, std::string input) : socket_{socket}, uri_(std::move(input)) {}
    int socket_{0};
    std::string uri_;
};

struct ResponseTcp {
    explicit ResponseTcp(std::string text) : text(std::move(text)) {}
    std::string text;
    [[nodiscard]] std::string to_string() const {return text;}
};

#include "ServerTcp.impl.hxx"

#endif //TESTSERVER__SERVERTCP_HXX_
