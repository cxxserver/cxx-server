//
// Created by SERGEY SUVOROV on 21/05/2022.
//

#include "Logging.hxx"

namespace expr = boost::log::expressions;
namespace attrs = boost::log::attributes;
namespace sinks = boost::log::sinks;
namespace logging = boost::log;

void Logging::init() {
    // Create a text file sink
    using file_sink = sinks::synchronous_sink< sinks::text_multifile_backend >;
    boost::shared_ptr< file_sink > sink(new file_sink);
    logging::add_common_attributes();

    // Set up how the file names will be generated
    sink->locked_backend()->set_file_name_composer(sinks::file::as_file_name_composer(
        expr::stream << std::getenv("LOG_PATH") << expr::attr< std::string >("Tag") << ".log"));
    sink->set_formatter
        (
            expr::stream
                << expr::format_date_time< boost::posix_time::ptime >("TimeStamp", "%Y-%m-%d %H:%M:%S")
                << ": <" << logging::trivial::severity
                << "> " << expr::smessage
        );

    // Add it to the core
    logging::core::get()->add_sink(sink);
}