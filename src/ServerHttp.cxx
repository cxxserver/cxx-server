//
// Created by SERGEY SUVOROV on 02/05/2022.
//

#include "ServerHttp.hxx"

RequestHttp::methodMap_t RequestHttp::methodMap_ = {{"GET", RequestHttp::Type::kGet},
                                                    {"POST", RequestHttp::Type::kPost},
                                                    {"DELETE", RequestHttp::Type::kDelete}};

RequestHttp::RequestHttp(const int socket, const std::string& input) : socket_(socket) {
    // parse the first line
    std::string line = input.substr(0, input.find('\n'));
    std::size_t start = 0;
    std::size_t end = line.find(' ');
    std::string method = line.substr(start, end - start);

    method_ = methodMap_[method];
    start = end + 1;
    end = line.find('?');

    if (end != std::string::npos) {
        // there is a query
        uri_ = line.substr(start, end - start);
        data_ = parseData(
            line.substr(end + 1,
                        line.find(' ', end + 1) - end - 1)
        );
    } else {
        end = line.find(' ', start);
        uri_ = line.substr(start, end - start);
    }
}

RequestHttp::dataMap RequestHttp::parseData(const std::string& input) {
    std::size_t start = -1;
    RequestHttp::dataMap map;
    auto pos = input.find('=', 0);
    while (pos != std::string::npos) {
        auto end = input.find('&', start + 1);
        auto key = input.substr(start + 1, pos - start - 1);
        auto value = input.substr(pos + 1, end - pos - 1);
        map[key] = value;
        start = end;
        pos = input.find('=', start);
    }

    return map;
}

std::string ResponseHttp::to_string() const {
    std::stringstream response;
    response << "HTTP/1.1 " << std::to_string(code_) << " OK\r\n"
         << "Version: HTTP/1.1\r\n"
         << "Content-Type: text/html; charset=utf-8\r\n"
         << "Content-Length: " << text_.length()
         << "\r\n\r\n"
         << text_;

    return response.str();
}

std::ostream& operator<<(std::ostream& o, const RequestHttp::dataMap& map) {
    o << "{";
    for (const auto& [key, value] : map) {
        o << key + ": " + value + ", ";
    }
    o << "}";
    return o;
}
