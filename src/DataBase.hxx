//
// Created by SERGEY SUVOROV on 21/05/2022.
//

#ifndef TESTSERVER_SRC_DATABASE_HXX_
#define TESTSERVER_SRC_DATABASE_HXX_

#include <pqxx/pqxx>
#include "Logging.hxx"

class DataBase {
    std::string connection_;
    std::shared_ptr<pqxx::connection> conn_;
    boost::log::sources::severity_logger<boost::log::trivial::severity_level> slg_;
 public:
    DataBase() = default;
    bool init(std::string connection);
    bool connect();
    bool disconnect();

    bool update(const std::string& sql);
};

#endif //TESTSERVER_SRC_DATABASE_HXX_
