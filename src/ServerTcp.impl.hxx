//
// Created by SERGEY SUVOROV on 02/05/2022.
//

#include <iostream>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <cstring>

#include <unistd.h>

using namespace boost::log::trivial;

template <typename request_t>
ServerTcp<request_t>::ServerTcp(requests_list_ptr dq) : dq_(dq) {
    namespace attrs = boost::log::attributes;
    slg_.add_attribute("Tag", attrs::constant<std::string >("Server"));
}

template <typename request_t>
int ServerTcp<request_t>::init() {
    const char portNum[5] = "2050";
    const unsigned int backLog = 8;

    addrinfo hints{}, *res{nullptr}, *p{nullptr};
    memset(&hints, 0, sizeof(hints));

    hints.ai_family   = AF_UNSPEC;    // don't specify which IP version to use yet
    hints.ai_socktype = SOCK_STREAM;  // SOCK_STREAM refers to TCP, SOCK_DGRAM will be?
    hints.ai_flags    = AI_PASSIVE;

    int gAddRes = getaddrinfo(nullptr, portNum, &hints, &res);
    if (gAddRes != 0) {
        BOOST_LOG_SEV(slg_, error) << gai_strerror(gAddRes);
    }

    unsigned int numOfAddr = 0;
    char ipStr[INET6_ADDRSTRLEN];

    for (p = res; p != nullptr; p = p->ai_next) {
        void *addr;
        std::string ipVer;

        // if address is ipv4 address
        if (p->ai_family == AF_INET) {
            ipVer             = "IPv4";
            auto *ipv4 = reinterpret_cast<sockaddr_in *>(p->ai_addr);
            addr              = &(ipv4->sin_addr);
            ++numOfAddr;
        }

            // if address is ipv6 address
        else {
            ipVer              = "IPv6";
            auto *ipv6 = reinterpret_cast<sockaddr_in6 *>(p->ai_addr);
            addr               = &(ipv6->sin6_addr);
            ++numOfAddr;
        }

        // convert IPv4 and IPv6 addresses from binary to text form
        inet_ntop(p->ai_family, addr, ipStr, sizeof(ipStr));
        BOOST_LOG_SEV(slg_, info) << "(" << numOfAddr << ") " << ipVer << " : " << ipStr
                                  << std::endl;
    }

    // if no addresses found :(
    if (!numOfAddr) {
        BOOST_LOG_SEV(slg_, error) << "Found no host address to use";
        return -3;
    }

    p = res;

    // let's create a new socket, socketFD is returned as descriptor
    // man socket for more information
    // these calls usually return -1 as result of some error
    sockFD_ = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
    if (sockFD_ == -1) {
        BOOST_LOG_SEV(slg_, error) << "Error while creating socket";
        freeaddrinfo(res);
        return -4;
    }


    // Let's bind address to our socket we've just created
    int bindR = bind(sockFD_, p->ai_addr, p->ai_addrlen);
    if (bindR == -1) {
        BOOST_LOG_SEV(slg_, error) << "Error while binding socket";
        BOOST_LOG_SEV(slg_, error) << std::strerror(errno);
        // if some error occurs, make sure to close socket and free resources
        close(sockFD_);
        freeaddrinfo(res);
        return -5;
    }

    // finally start listening for connections on our socket
    int listenR = listen(sockFD_, backLog);
    if (listenR == -1) {
        BOOST_LOG_SEV(slg_, error) << "Error while Listening on socket";

        // if some error occurs, make sure to close socket and free resources
        close(sockFD_);
        freeaddrinfo(res);
        return -6;
    }
    BOOST_LOG_SEV(slg_, info) << "Server is initialised\n";
    return 0;
}

template <typename request_t>
void ServerTcp<request_t>::run() {
    while (true) {
        sockaddr_storage client_addr{};
        socklen_t client_addr_size = sizeof(client_addr);
        int newFD
            = accept(sockFD_, (sockaddr *) &client_addr, &client_addr_size);

        BOOST_LOG_SEV(slg_, info) << "New socket accepted " << newFD << std::endl;

        if (newFD == -1) {
            BOOST_LOG_SEV(slg_, error) << "Error while Accepting on socket " << sockFD_;
            continue;
        }

        int buffSize = 2000;
        std::string buff(buffSize, 0);

        auto len = recv(newFD, &buff[0], buffSize-1, 0);
        BOOST_LOG_SEV(slg_, info) << "Socket " << newFD << " Len " << len << std::endl;

        std::string request;
        for (auto i = 0; i < buffSize; ++i) {
            if (buff[i] == '\0') {
                break;
            }
            request += buff[i];
        }

        dq_->push(request_t(newFD, request));
    }
}

template <typename request_t>
template <typename response_t>
void ServerTcp<request_t>::response(const int socket, const response_t& resp) {
    BOOST_LOG_SEV(slg_, info) << "Sending to socket " << socket << std::endl;
    send(socket, resp.to_string().c_str(), resp.to_string().length(), 0);
    close(socket);
}


