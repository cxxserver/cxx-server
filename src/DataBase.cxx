//
// Created by SERGEY SUVOROV on 21/05/2022.
//

#include "DataBase.hxx"

#include <iostream>
#include <utility>

using namespace boost::log::trivial;

bool DataBase::init(std::string connection) {
    namespace attrs = boost::log::attributes;
    slg_.add_attribute("Tag", attrs::constant<std::string >("DB"));
    connection_ = std::move(connection);
    if (connect()) {
        disconnect();
        BOOST_LOG_SEV(slg_, info) << "DB init success";
        return true;
    }
    BOOST_LOG_SEV(slg_, error) << "DB init fail";
    return false;
}

bool DataBase::connect() {
    if (conn_ && conn_->is_open()) {
        BOOST_LOG_SEV(slg_, info) << "DB connect success";
        return true;
    }
    conn_ = std::make_shared<pqxx::connection>(connection_.c_str());
    if (conn_->is_open()) {
        BOOST_LOG_SEV(slg_, info) << "DB connect success";
        return true;
    }
    BOOST_LOG_SEV(slg_, error) << "DB connect fail";
    return false;
}

bool DataBase::disconnect() {
    if (conn_->is_open()) {
        BOOST_LOG_SEV(slg_, info) << "DB disconnect success";
        conn_->close();
        return true;
    }
    BOOST_LOG_SEV(slg_, error) << "DB disconnect fail";
    return false;
}

bool DataBase::update(const std::string& sql) {
    if (!connect())
        return false;
    try {
        pqxx::work work(*conn_);
        work.exec(sql);
        work.commit();
        disconnect();
        BOOST_LOG_SEV(slg_, info) << "DB update success";
        return true;
    } catch (std::exception& e) {
        BOOST_LOG_SEV(slg_, error) << "DB update fail " << e.what();
        return false;
    }
}