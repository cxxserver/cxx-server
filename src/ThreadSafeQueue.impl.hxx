//
// Created by SERGEY SUVOROV on 09/05/2022.
//

template <typename request_t>
request_t ThreadSafeQueue<request_t>::pop() {
    std::unique_lock ul(mu_);
    while (queue_.empty()) {
        cv_.wait(ul);
    }
    auto result = queue_.front();
    queue_.erase(queue_.begin());
    return result;
}

template <typename request_t>
void ThreadSafeQueue<request_t>::push(request_t request) {
    std::lock_guard lg(mu_);
    queue_.push_back(request);
    cv_.notify_all();
}

template <typename request_t>
void ThreadSafeQueue<request_t>::clear() {
    std::lock_guard lg(mu_);
    queue_.clear();
}
