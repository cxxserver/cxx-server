//
// Created by SERGEY SUVOROV on 21/05/2022.
//

#ifndef TESTSERVER_SRC_LOGGING_HXX_
#define TESTSERVER_SRC_LOGGING_HXX_

#include <boost/log/trivial.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/basic_logger.hpp>
#include <boost/log/attributes/constant.hpp>
#include <boost/log/sinks/text_multifile_backend.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>

namespace Logging {
    void init();
};

#endif //TESTSERVER_SRC_LOGGING_HXX_
