//
// Created by SERGEY SUVOROV on 09/05/2022.
//

#ifndef TESTSERVER__THREADSAFEQUEUE_HXX_
#define TESTSERVER__THREADSAFEQUEUE_HXX_

#include <mutex>
#include <condition_variable>
#include <deque>

template <typename request_t>
class ThreadSafeQueue {
    std::mutex mu_;
    std::condition_variable cv_;

    std::deque<request_t> queue_;
 public:
    request_t pop();
    void push(request_t request);
    void clear();

};

#include "ThreadSafeQueue.impl.hxx"

#endif //TESTSERVER__THREADSAFEQUEUE_HXX_
