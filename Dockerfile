FROM  ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y cmake libboost-log-dev git \
    g++ postgresql libpq-dev postgresql-server-dev-all libboost-log-dev

RUN git clone https://github.com/jtv/libpqxx.git && \
    mkdir build && cd build && \
    cmake ../libpqxx/ && make -j6 && make install

RUN mkdir app
COPY . app

WORKDIR app

RUN mkdir build || true && cd build && rm -rf * && \
    cmake .. && \
    make -j5